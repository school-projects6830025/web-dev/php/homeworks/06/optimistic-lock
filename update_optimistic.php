<?php
require 'db.php';
/** @var \PDO $db */
require 'admin_required.php';

$stmt = $db->prepare('SELECT * FROM goods WHERE id=:id');
$stmt->execute([':id' => @$_REQUEST['id']]);
$goods = $stmt->fetch(PDO::FETCH_ASSOC);

if (!$goods) {
    die("Unable to find goods!");
}

if (isset($_POST['approve']) && !empty($_POST['newName']) && !empty($_POST['newDescription']) && !empty($_POST['newPrice'])) {
    $stmt = $db->prepare('UPDATE goods SET name=:name, description=:description, price=:price, last_updated_at=now() WHERE id=:id LIMIT 1;');
    $stmt->execute([
        ':name' => $_POST['newName'],
        ':description' => $_POST['newDescription'],
        ':price' => $_POST['newPrice'],
        ':id' => $goods['id']
    ]);

    header('Location: index.php');
    exit();
}

$name = $goods['name'];
$description = $goods['description'];
$price = $goods['price'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>PHP Shopping App</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
<?php
if (!empty($_POST['submit'])) {
    $formErrors = '';
    $name = $_POST['name'];
    $description = $_POST['description'];
    $price = floatval($_POST['price']);

    if (empty($formErrors)) {
        if ($_POST['last_updated_at'] != $goods['last_updated_at']) {
            ?>

            <h1>The goods were updated by someone else in the meantime!</h1>

            <label for="prev">Current data</label>
            <table id="prev">
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Description</th>
                </tr>

                <tr>
                    <td><?php echo htmlspecialchars($goods['name']); ?></td>
                    <td class="right"><?php echo $goods['price']; ?></td>
                    <td><?php echo htmlspecialchars($goods['description']); ?></td>
                </tr>
            </table>

            <label for="now">Your data</label>
            <form method="post">
                <table id="now">
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Description</th>
                    </tr>

                    <tr>
                        <td><?php echo htmlspecialchars($name); ?></td>
                        <td class="right"><?php echo $price; ?></td>
                        <td><?php echo htmlspecialchars($description); ?></td>
                    </tr>
                </table>
                <input type="hidden" name="newName" value="<?php echo htmlspecialchars($name); ?>"/>
                <input type="hidden" name="newPrice" value="<?php echo $price; ?>"/>
                <input type="hidden" name="newDescription" value="<?php echo htmlspecialchars($description); ?>"/>

                <a href="update_optimistic.php?id=<?php echo $goods['id']; ?>">Cancel</a>
                <button type="submit" name="approve">Approve</button>
            </form>

            <?php
        } else {
            $stmt = $db->prepare('UPDATE goods SET name=:name, description=:description, price=:price, last_updated_at=now() WHERE id=:id LIMIT 1;');
            $stmt->execute([
                ':name' => $name,
                ':description' => $description,
                ':price' => $price,
                ':id' => $goods['id']
            ]);

            header('Location: index.php');
            exit();
        }
    }
} else {
    include 'navbar.php'
    ?>

    <h1>Update goods</h1>

    <?php
    if (!empty($formErrors)) {
        echo '<p style="color:red;">' . $formErrors . '</p>';
    }
    ?>

    <form method="post">
        <label for="name">Name</label>
        <br/>
        <input type="text" name="name" id="name" value="<?php echo htmlspecialchars(@$name); ?>" required>
        <br/><br/>

        <label for="price">Price</label>
        <br/>
        <input type="number" min="0" step="0.01" name="price" id="price" required
               value="<?php echo htmlspecialchars(@$price) ?>">
        <br/><br/>

        <label for="description">Description</label>
        <br/>
        <textarea name="description"
                  id="description"><?php echo htmlspecialchars(@$description) ?></textarea>
        <br/><br/>

        <input type="hidden" name="id" value="<?php echo $goods['id']; ?>"/>

        <input type="hidden" name="last_updated_at" value="<?php echo $goods['last_updated_at']; ?>">

        <input type="submit" name="submit" value="Save"/> or <a href="index.php">Cancel</a>
    </form>
    <?php
}
?>
</body>
</html>