<?php
require 'db.php';
/** @var \PDO $db */
require 'admin_required.php';

$stmt = $db->prepare('SELECT goods.*, eshop.email, now() > last_edit_starts_at + INTERVAL 5 MINUTE AS edit_expired FROM goods LEFT JOIN eshop ON eshop.id=goods.last_edit_starts_by_user WHERE goods.id=:id');
$stmt->execute([':id' => @$_REQUEST['id']]);
$goods = $stmt->fetch(PDO::FETCH_ASSOC);

if (!$goods) {
    die("Unable to find goods!");
}

$name = $goods['name'];
$description = $goods['description'];
$price = $goods['price'];

if (
    !empty($goods["last_edit_starts_by_user"]) &&
    $goods["last_edit_starts_by_user"] != $currentUser['id'] &&
    !$goods['edit_expired']
) {
    die("The goods is currently edited by " . $goods['email']);
}

$stmt = $db->prepare("UPDATE goods SET last_edit_starts_at=NOW(), last_edit_starts_by_user=:user WHERE id=:id");
$stmt->execute([':user' => $currentUser["id"], ':id' => $_GET['id']]);

if (!empty($_POST)) {
    $formErrors = '';
    $name = $_POST['name'];
    $description = $_POST['description'];
    $price = floatval($_POST['price']);

    if (empty($formErrors)) {
        $stmt = $db->prepare('UPDATE goods SET name=:name, description=:description, price=:price, last_edit_starts_by_user=NULL, last_edit_starts_at=NULL WHERE id=:id LIMIT 1;');
        $stmt->execute([
            ':name' => $name,
            ':description' => $description,
            ':price' => $price,
            ':id' => $goods['id']
        ]);

        header('Location: index.php');
        exit();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>PHP Shopping App</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
<?php include 'navbar.php' ?>

<h1>Update goods</h1>

<?php
if (!empty($formErrors)) {
    echo '<p style="color:red;">' . $formErrors . '</p>';
}
?>

<form method="post">
    <label for="name">Name</label><br/>
    <input type="text" name="name" id="name" value="<?php echo htmlspecialchars(@$name); ?>" required><br/><br/>

    <label for="price">Price<br/>
        <input type="number" min="0" name="price" id="price" required
               value="<?php echo htmlspecialchars(@$price) ?>"><br/><br/>

        <label for="description">Description</label><br/>
        <textarea name="description" id="description"><?php echo htmlspecialchars(@$description) ?></textarea><br/><br/>
        <br/>

        <input type="hidden" name="id" value="<?php echo $goods['id']; ?>"/>
        <input type="submit" value="Save"/> or <a href="index.php">Cancel</a>
</form>
</body>
</html>
