<?php
  session_start();
  require 'db.php';
  /** @var \PDO $db */
	
  if (!empty($_POST)) {
      $email = @$_POST['email'];
      $password = @$_POST['password'];

      $stmt = $db->prepare("SELECT * FROM eshop WHERE email = ? LIMIT 1");
      $stmt->execute([$email]);
      if ( ($existingUser=$stmt->fetch(PDO::FETCH_ASSOC)) && password_verify($password, @$existingUser['password']) ) {
        $_SESSION['user_id'] = $existingUser['id'];
        header('Location: index.php');
      } else {
        $formError="Invalid user or password!";
      }
  }
  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>PHP Shopping App</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
  </head>
  <body>
    <h1>PHP Shopping App</h1>
    <h2>Sign in</h2>
    <?php
      if (!empty($formError)) {
        echo '<p style="color:red;">'.$formError.'</p>';
      }
    ?>

    <form method="post">
      <label for="email">Your Email</label>
        <br/>
      <input type="text" name="email" id="email" value="<?php echo htmlspecialchars(@$_POST['email'] ?? '')?>">
        <br/><br/>

      <label for="password">Password</label>
        <br/>
      <input type="password" name="password" id="password" value="">
        <br/><br/>

      <input type="submit" value="Sign in">
    </form>
    <br/>

    <a href="signup.php">Don't have an account yet? Sign up!</a>
  </body>
</html>
