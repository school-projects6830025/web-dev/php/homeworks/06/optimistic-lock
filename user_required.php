<?php
session_start();
/** @var \PDO $db */

if (!isset($_SESSION["user_id"])) {
    header('Location: signin.php');
    die();
}

$stmt = $db->prepare("SELECT * FROM eshop WHERE id = ? LIMIT 1");
$stmt->execute([$_SESSION["user_id"]]);

$currentUser = $stmt->fetch(PDO::FETCH_ASSOC);

if (!$currentUser) {
    session_destroy();
    header('Location: index.php');
    die();
}
