<?php
  session_start();
  require 'db.php';
  /** @var \PDO $db */
	
  if (!empty($_POST)) {
    $email = @$_POST['email'];
    $password = @$_POST['password'];
    $passwordHash = password_hash($password, PASSWORD_DEFAULT);

    $stmt = $db->prepare("INSERT INTO eshop(email, password) VALUES (?, ?)");
    $stmt->execute([$email, $passwordHash]);

    $stmt = $db->prepare("SELECT id FROM eshop WHERE email = ? LIMIT 1");
    $stmt->execute([$email]);

    $_SESSION['user_id'] = (int)$stmt->fetchColumn();
  	header('Location: index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>PHP Shopping App</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
  </head>
  <body>
		<h1>PHP Shopping App</h1>
		<h2>New Signup</h2>
		<form method="post">
      <label for="email">Your Email</label>
            <br/>
			<input type="text" name="email" id="email" required value="<?php echo htmlspecialchars(@$_POST['email'] ?? '');?>">
            <br/><br/>
	  
      <label for="password">New Password</label>
            <br/>
			<input type="password" name="password" id="password" required value="">
            <br/><br/>
	
			<input type="submit" value="Create Account"> or <a href="index.php">Cancel</a>
		</form>
  </body>
</html>
