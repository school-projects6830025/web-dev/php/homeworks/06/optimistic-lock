<?php
  require 'db.php';
  /** @var \PDO $db */

  require 'admin_required.php';

  $stmt = $db->prepare("DELETE FROM goods WHERE id=?");
  $stmt->execute([$_GET['id']]);

  header('Location: index.php');