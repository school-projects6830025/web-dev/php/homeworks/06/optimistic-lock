<?php
  require 'db.php';
  /** @var \PDO $db */
  require 'user_required.php';

  if (isset($_GET['offset'])) {
    $offset = (int)$_GET['offset'];
  } else {
    $offset = 0;
  }

  $count = $db->query("SELECT COUNT(id) FROM goods")->fetchColumn();

  $stmt = $db->prepare("SELECT * FROM goods ORDER BY id DESC LIMIT 10 OFFSET ?");
  $stmt->bindValue(1, $offset, PDO::PARAM_INT);
  $stmt->execute();

  $goods = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>PHP Shopping App</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
  </head>
  <body>
    <?php include 'navbar.php' ?>
    <h1>Goods index</h1>
    Total goods: <strong><?php echo $count; ?></strong>
    <br/><br/>
  
    <a href="new.php">New Good</a>
    <br/><br/>
  
    <?php if ($count>0) { ?>
      <table>
        <tr>
          <th></th>
          <th>Name</th>
          <th>Price</th>
          <th>Description</th>
          <th></th>
        </tr>
  
        <?php foreach($goods as $row) { ?>
          <tr>
            <td class="center">
              <a href='buy.php?id=<?php echo $row['id']; ?>'>Buy</a>
            </td>

            <td><?php echo htmlspecialchars($row['name']); ?></td>
            <td class="right"><?php echo $row['price']; ?></td>
            <td><?php echo htmlspecialchars($row['description']); ?></td>

            <td class="center">
              <a href='update_optimistic.php?id=<?php echo $row['id']; ?>'>Edit (optimistic lock)</a> |
              <a href='update_pessimistic.php?id=<?php echo $row['id']; ?>'>Edit (pessimistic lock)</a> |
              <a href='delete.php?id=<?php echo $row['id']; ?>'>Delete</a>
            </td>
          </tr>
        <?php } ?>
      </table>
      <br/>

      <div class="pagination">
        <?php
          for($i=1; $i<=ceil($count/10); $i++){
            echo '<a class="'.($offset/10+1==$i?'active':'').'" href="index.php?offset='.(($i-1)*10).'">'.$i.'</a>';
          }
        ?>
      </div>
    <?php } ?>

  </body>
</html>

