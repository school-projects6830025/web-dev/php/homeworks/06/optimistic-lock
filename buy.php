<?php
  require 'db.php';
  /** @var \PDO $db  */

  require 'user_required.php';

  if (!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = [];
  }

  $stmt = $db->prepare("SELECT * FROM goods WHERE id=?");
  $stmt->execute([$_GET['id']]);
  $goods = $stmt->fetch();

  if (!$goods) {
    die("Unable to find goods!");
  }

  $_SESSION['cart'][] = $goods["id"];

  header('Location: cart.php');
