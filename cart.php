<?php
  require 'db.php';
  /** @var \PDO $db  */
  require 'user_required.php';

  $ids = @$_SESSION['cart'];

  if (is_array($ids) && count($ids)>0) {
    $question_marks = str_repeat('?,', count($ids) - 1).'?';

    $stmt = $db->prepare("SELECT * FROM goods WHERE id IN ($question_marks) ORDER BY name");
    $stmt->execute(array_values($ids));
    $goods = $stmt->fetchAll();
  }

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>My shopping cart - PHP Shopping App</title>
    <link rel="stylesheet" type="text/css" href="./styles.css">
  </head>
  <body>
	
	  <?php include 'navbar.php' ?>
		
	  <h1>My shopping cart</h1>
	
    Total goods selected: <strong><?php echo (!empty($goods)?count($goods):'0'); ?></strong>
	
	  <br/><br/>
	
    <a href="index.php">Back to the goods</a>
	
	  <br/><br/>

	  <?php
      if (!empty($goods)) {
        $sum=0;
        echo '<table>
                <thead>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>';

        foreach ($goods as $good) {
          echo '  <tr>
                    <td class="center">
                      <a href="remove.php?id='.$good['id'].'">Remove</a>
                    </td>
                    <td>'.htmlspecialchars($good['name']).'</td>
                    <td class="right">'.$good['price'].'</td>
                    <td>'.htmlspecialchars($good['description']).'</td>
                  </tr>';
          $sum+=$good['price'];
        }

        echo '  </tbody>
                <tfoot>
                  <tr>
                    <td>SUM</td>
                    <td></td>
                    <td class="right">'.$sum.'</td>
                    <td></td>
                  </tr>
                </tfoot>                
              </table>';

      } else {
        echo 'No goods yet.';
      }
    ?>
  </body>
</html>