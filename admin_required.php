<?php
  require 'user_required.php';

  if(empty($currentUser) || ($currentUser['role']!='admin')) {
    die('Tato stránka je dostupná pouze administrátorům.');
  }